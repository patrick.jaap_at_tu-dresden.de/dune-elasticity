
#include <config.h>

#include <iostream>

#include <dune/common/test/testsuite.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/timer.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>

#include <dune/elasticity/assemblers/feassembler.hh>
#include <dune/elasticity/assemblers/localadolcstiffness.hh>
#include <dune/elasticity/assemblers/localhyperdualstiffness.hh>
#include <dune/elasticity/materials/stvenantkirchhoffdensity.hh>
#include <dune/elasticity/materials/localintegralenergy.hh>

using namespace Dune;

template<int dim>
TestSuite hyperdualEqualsADOL_C()
{
  TestSuite t;

  using Vector = BlockVector<FieldVector<double, dim>>;
  using Matrix = BCRSMatrix<FieldMatrix<double,dim,dim>>;

  using GridType = UGGrid<dim>;

  std::shared_ptr<GridType> grid;

  FieldVector<double,dim> lower(0), upper(1);

  std::array<unsigned int,dim> elements;
  elements.fill(10);

  grid = StructuredGridFactory<GridType>::createCubeGrid(lower, upper, elements);

  using GridView = typename GridType::LeafGridView;
  GridView gridView = grid->leafGridView();

  using namespace Dune::Functions::BasisFactory;
  auto basis = makeBasis(
    gridView,
    power<dim>(
      lagrange<1>()
  ));

  auto localView = basis.localView();
  using LocalView = decltype(localView);
  using Basis = decltype(basis);

  std::shared_ptr<Elasticity::LocalFEStiffness<LocalView,double>> localStiffness;

  std::string parameterString = "mu = 2.7191e+6\nlambda = 4.4364e+6";
  std::istringstream stream(parameterString);
  ParameterTree parameterSet;
  ParameterTreeParser::readINITree(stream, parameterSet);

  auto hyperdualElasticDensity = std::make_shared<Elasticity::StVenantKirchhoffDensity<dim,hyperdual>>(parameterSet);
  auto adolcElasticDensity     = std::make_shared<Elasticity::StVenantKirchhoffDensity<dim,adouble>>(parameterSet);

  auto hyperdualElasticEnergy = std::make_shared<Elasticity::LocalIntegralEnergy<LocalView, hyperdual>>(hyperdualElasticDensity);
  auto adolcElasticEnergy = std::make_shared<Elasticity::LocalIntegralEnergy<LocalView, adouble>>(adolcElasticDensity);

  auto hyperdualLocalStiffness = std::make_shared<Elasticity::LocalHyperDualStiffness<LocalView>>(hyperdualElasticEnergy);
  auto adolcLocalStiffness = std::make_shared<Elasticity::LocalADOLCStiffness<LocalView>>(adolcElasticEnergy);

  Elasticity::FEAssembler<Basis,Vector> hyperdualAssembler(basis, hyperdualLocalStiffness);
  Elasticity::FEAssembler<Basis,Vector> adolcAssembler(basis, adolcLocalStiffness);

  Matrix hyperdualHessian, adolcHessian;
  Vector hyperdualGradient, adolcGradient;

  Vector x(basis.size());
  x = 42.;

  Timer timer;
  hyperdualAssembler.assembleGradientAndHessian(x,hyperdualGradient,hyperdualHessian);
  std::cout <<  dim <<"D: hyperdual assembler took " << timer.elapsed() << " sec." << std::endl;
  timer.reset();
  adolcAssembler.assembleGradientAndHessian(x,adolcGradient,adolcHessian);
  std::cout << dim << "D: ADOL-C assembler took " << timer.elapsed() << " sec." << std::endl;


  hyperdualHessian -= adolcHessian;
  hyperdualGradient -= adolcGradient;

  // check the relative error
  t.check(hyperdualHessian.frobenius_norm()/adolcHessian.frobenius_norm() < 1e-12 && hyperdualGradient.two_norm()/adolcGradient.two_norm() < 1e-12);

  return t;

}

int main(int argc, char **argv)
{
  Dune::MPIHelper::instance(argc, argv);

  TestSuite t;

  t.subTest(hyperdualEqualsADOL_C<2>());
  t.subTest(hyperdualEqualsADOL_C<3>());

  return t.exit();
}
