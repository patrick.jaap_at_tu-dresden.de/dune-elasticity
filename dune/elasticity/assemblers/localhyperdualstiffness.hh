#pragma once

#include <dune/common/fmatrix.hh>
#include <dune/istl/matrix.hh>

#include <dune/elasticity/assemblers/localfestiffness.hh>
#include <dune/elasticity/common/hyperdual.hh>

namespace Dune::Elasticity {

/** \brief Assembles energy gradient and Hessian using Hyperdual Numbers 
 */
template<class LocalView>
class LocalHyperDualStiffness
    : public LocalFEStiffness<LocalView,double>
{
    enum {gridDim=LocalView::GridView::dimension};

public:

    // accepts hyperdual only
    LocalHyperDualStiffness(std::shared_ptr<Dune::Elasticity::LocalEnergy<LocalView, hyperdual>> energy)
    : localEnergy_(energy)
    {}

    /** \brief Compute the energy at the current configuration */
    virtual double energy (const LocalView& localView,
                           const std::vector<double>& localConfiguration) const;

    /** \brief Assemble the local stiffness matrix at the current position
     *
     *     This uses the automatic differentiation using hyperdual numbers
     */
    virtual void assembleGradientAndHessian(const LocalView& localView,
                                            const std::vector<double>& localConfiguration,
                                            std::vector<double>& localGradient,
                                            Dune::Matrix<double>& localHessian);

    std::shared_ptr<Dune::Elasticity::LocalEnergy<LocalView, hyperdual>> localEnergy_; 

};


template<class LocalView>
double LocalHyperDualStiffness<LocalView>::
energy(const LocalView& localView,
       const std::vector<double>& localConfiguration) const
{
    double pureEnergy;

    std::vector<hyperdual> localHyperDualConfiguration(localConfiguration.size());
    hyperdual energy;
    
    for (size_t i=0; i<localConfiguration.size(); i++)
        localHyperDualConfiguration[i] = hyperdual(localConfiguration[i]);        
        
    energy = localEnergy_->energy(localView,localHyperDualConfiguration);

    pureEnergy = energy.real();

    return pureEnergy;
}


template<class LocalView>
void LocalHyperDualStiffness<LocalView>::
assembleGradientAndHessian(const LocalView& localView,
                           const std::vector<double>& localConfiguration,
                           std::vector<double>& localGradient,
                           Dune::Matrix<double>& localHessian
                          )
{
    size_t nDoubles = localConfiguration.size(); 

    localGradient.resize(nDoubles);

    localHessian.setSize(nDoubles,nDoubles);
    // Create hyperdual vector localHyperDualConfiguration = double vector localConfiguration
    std::vector<hyperdual> localHyperDualConfiguration(nDoubles);
    for (size_t i=0; i<nDoubles; i++)
       localHyperDualConfiguration[i] = hyperdual(localConfiguration[i]);
    
    // Compute gradient and hessian (symmetry is used) using hyperdual function evaluation
    // localHyperDualConfiguration puts Ones in the eps1 and eps2 component to evaluate the different partial derivatives
    for (size_t i=0; i<nDoubles; i++) 
    {
      localHyperDualConfiguration[i].seteps1(1.0);
      localHyperDualConfiguration[i].seteps2(1.0);
    
      hyperdual temp = localEnergy_->energy(localView, localHyperDualConfiguration);
      localGradient[i] = temp.eps1();                   
      localHessian[i][i] = temp.eps1eps2();
      
      localHyperDualConfiguration[i].seteps2(0.0);

      for (size_t j=i+1; j<nDoubles; j++) 
      { 
        localHyperDualConfiguration[j].seteps2(1.0);
        localHessian[i][j] = localEnergy_->energy(localView, localHyperDualConfiguration).eps1eps2();
        localHessian[j][i] = localHessian[i][j];        // Use symmetry of hessian 
        localHyperDualConfiguration[j].seteps2(0.0);
      }
        
      localHyperDualConfiguration[i].seteps1(0.0);
    }

}

} // namespace Dune::Elasticity
